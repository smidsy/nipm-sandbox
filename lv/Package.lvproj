﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="source" Type="Folder">
			<Item Name="Noffz.UTP.ESF.lv14.x86.exe" Type="Document" URL="../source/Noffz.UTP.ESF.lv14.x86.exe"/>
			<Item Name="unins000.exe" Type="Document" URL="../source/unins000.exe"/>
		</Item>
		<Item Name="Unpack.vi" Type="VI" URL="../Unpack.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Get System Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/Get System Directory.vi"/>
				<Item Name="NIPM_API.lvlib" Type="Library" URL="/&lt;vilib&gt;/National Instruments/NIPM API (Beta)/NIPM_API.lvlib"/>
				<Item Name="System Directory Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/System Directory Type.ctl"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="UTP ESF for LabVIEW 2014 (x86)" Type="{E661DAE2-7517-431F-AC41-30807A3BDA38}">
				<Property Name="NIPKG_license" Type="Ref"></Property>
				<Property Name="NIPKG_releaseNotes" Type="Str"></Property>
				<Property Name="PKG_actions.Count" Type="Int">1</Property>
				<Property Name="PKG_actions[0].Arguments" Type="Str">/SILENT</Property>
				<Property Name="PKG_actions[0].NIPKG.HideConsole" Type="Bool">true</Property>
				<Property Name="PKG_actions[0].NIPKG.IgnoreErrors" Type="Bool">false</Property>
				<Property Name="PKG_actions[0].NIPKG.Schedule" Type="Str">post</Property>
				<Property Name="PKG_actions[0].NIPKG.Step" Type="Str">install</Property>
				<Property Name="PKG_actions[0].NIPKG.Target.Child" Type="Str"></Property>
				<Property Name="PKG_actions[0].NIPKG.Target.Destination" Type="Str">root_7</Property>
				<Property Name="PKG_actions[0].NIPKG.Target.Source" Type="Ref">/My Computer/source/Noffz.UTP.ESF.lv14.x86.exe</Property>
				<Property Name="PKG_actions[0].NIPKG.Wait" Type="Bool">true</Property>
				<Property Name="PKG_actions[0].Type" Type="Str">NIPKG.Executable</Property>
				<Property Name="PKG_actions[1].Arguments" Type="Str">/SILENT</Property>
				<Property Name="PKG_actions[1].NIPKG.HideConsole" Type="Bool">true</Property>
				<Property Name="PKG_actions[1].NIPKG.IgnoreErrors" Type="Bool">true</Property>
				<Property Name="PKG_actions[1].NIPKG.Schedule" Type="Str">pre</Property>
				<Property Name="PKG_actions[1].NIPKG.Step" Type="Str">uninstall</Property>
				<Property Name="PKG_actions[1].NIPKG.Target.Child" Type="Str"></Property>
				<Property Name="PKG_actions[1].NIPKG.Target.Destination" Type="Str">{DCB031E9-BE35-496C-8799-282D0DE62DB9}</Property>
				<Property Name="PKG_actions[1].NIPKG.Target.Source" Type="Ref"></Property>
				<Property Name="PKG_actions[1].NIPKG.Wait" Type="Bool">true</Property>
				<Property Name="PKG_actions[1].Type" Type="Str">NIPKG.Executable</Property>
				<Property Name="PKG_autoIncrementBuild" Type="Bool">true</Property>
				<Property Name="PKG_autoSelectDeps" Type="Bool">false</Property>
				<Property Name="PKG_buildNumber" Type="Int">2</Property>
				<Property Name="PKG_buildSpecName" Type="Str">UTP ESF for LabVIEW 2014 (x86)</Property>
				<Property Name="PKG_dependencies.Count" Type="Int">1</Property>
				<Property Name="PKG_dependencies[0].Enhanced" Type="Bool">true</Property>
				<Property Name="PKG_dependencies[0].MaxVersion" Type="Str"></Property>
				<Property Name="PKG_dependencies[0].MaxVersionInclusive" Type="Bool">false</Property>
				<Property Name="PKG_dependencies[0].MinVersion" Type="Str"></Property>
				<Property Name="PKG_dependencies[0].MinVersionType" Type="Str">Inclusive</Property>
				<Property Name="PKG_dependencies[0].NIPKG.DisplayName" Type="Str">NI LabVIEW 2017 Runtime</Property>
				<Property Name="PKG_dependencies[0].Package.Name" Type="Str">ni-labview-2017-runtime-engine-x86</Property>
				<Property Name="PKG_dependencies[0].Package.Section" Type="Str">Programming Environments</Property>
				<Property Name="PKG_dependencies[0].Package.Synopsis" Type="Str">NI LabVIEW 2017 Runtime provides libraries and other files necessary to execute LabVIEW 2017-built applications and shared libraries. Includes NI Reports, 3D graph support, and a browser plug-in that enables clients to view and control front panels remotely using a browser.</Property>
				<Property Name="PKG_dependencies[0].Relationship" Type="Str">Required Dependency</Property>
				<Property Name="PKG_dependencies[0].Type" Type="Str">NIPKG</Property>
				<Property Name="PKG_description" Type="Str">UTP ESF for LabVIEW 2014 (x86)</Property>
				<Property Name="PKG_destinations.Count" Type="Int">4</Property>
				<Property Name="PKG_destinations[0].ID" Type="Str">{CC758DB3-7244-44DF-8DCA-42D97AE288A7}</Property>
				<Property Name="PKG_destinations[0].Subdir.Directory" Type="Str">uninst</Property>
				<Property Name="PKG_destinations[0].Subdir.Parent" Type="Str">{D376581B-1C4E-4F75-BD21-68D62569624F}</Property>
				<Property Name="PKG_destinations[0].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_destinations[1].ID" Type="Str">{D376581B-1C4E-4F75-BD21-68D62569624F}</Property>
				<Property Name="PKG_destinations[1].Subdir.Directory" Type="Str">UTP ESF Package</Property>
				<Property Name="PKG_destinations[1].Subdir.Parent" Type="Str">{DE432EF2-ACCD-402D-9ED6-B49F2E38DBAE}</Property>
				<Property Name="PKG_destinations[1].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_destinations[2].ID" Type="Str">{DCB031E9-BE35-496C-8799-282D0DE62DB9}</Property>
				<Property Name="PKG_destinations[2].Subdir.Directory" Type="Str">{6CB88087-2AC3-4D6F-9355-3DAC358CC9D5}</Property>
				<Property Name="PKG_destinations[2].Subdir.Parent" Type="Str">{CC758DB3-7244-44DF-8DCA-42D97AE288A7}</Property>
				<Property Name="PKG_destinations[2].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_destinations[3].ID" Type="Str">{DE432EF2-ACCD-402D-9ED6-B49F2E38DBAE}</Property>
				<Property Name="PKG_destinations[3].Subdir.Directory" Type="Str">Noffz</Property>
				<Property Name="PKG_destinations[3].Subdir.Parent" Type="Str">root_4</Property>
				<Property Name="PKG_destinations[3].Type" Type="Str">Subdir</Property>
				<Property Name="PKG_displayName" Type="Str">UTP ESF for LabVIEW 2014 (x86)</Property>
				<Property Name="PKG_displayVersion" Type="Str">1.2.0</Property>
				<Property Name="PKG_homepage" Type="Str"></Property>
				<Property Name="PKG_lvrteTracking" Type="Bool">false</Property>
				<Property Name="PKG_maintainer" Type="Str">NOFFZ Technologies GmbH &lt;info@noffz.com&gt;</Property>
				<Property Name="PKG_output" Type="Path">../builds/NI_AB_PROJECTNAME</Property>
				<Property Name="PKG_output.Type" Type="Str">relativeToCommon</Property>
				<Property Name="PKG_packageName" Type="Str">noffz.utp.esf.lv14.x86</Property>
				<Property Name="PKG_ProviderVersion" Type="Int">1810</Property>
				<Property Name="PKG_section" Type="Str">Add-Ons</Property>
				<Property Name="PKG_shortcuts.Count" Type="Int">1</Property>
				<Property Name="PKG_shortcuts[0].Destination" Type="Str">root_1</Property>
				<Property Name="PKG_shortcuts[0].Name" Type="Str">unins000</Property>
				<Property Name="PKG_shortcuts[0].Path" Type="Path">ESF (unins000)</Property>
				<Property Name="PKG_shortcuts[0].Target.Child" Type="Str"></Property>
				<Property Name="PKG_shortcuts[0].Target.Destination" Type="Str">{DCB031E9-BE35-496C-8799-282D0DE62DB9}</Property>
				<Property Name="PKG_shortcuts[0].Target.Source" Type="Ref">/My Computer/source/unins000.exe</Property>
				<Property Name="PKG_shortcuts[0].Type" Type="Str">NIPKG</Property>
				<Property Name="PKG_sources.Count" Type="Int">2</Property>
				<Property Name="PKG_sources[0].Destination" Type="Str">root_7</Property>
				<Property Name="PKG_sources[0].ID" Type="Ref">/My Computer/source/Noffz.UTP.ESF.lv14.x86.exe</Property>
				<Property Name="PKG_sources[0].Type" Type="Str">File</Property>
				<Property Name="PKG_sources[1].Destination" Type="Str">{DCB031E9-BE35-496C-8799-282D0DE62DB9}</Property>
				<Property Name="PKG_sources[1].ID" Type="Ref">/My Computer/source/unins000.exe</Property>
				<Property Name="PKG_sources[1].Type" Type="Str">File</Property>
				<Property Name="PKG_synopsis" Type="Str">UTP ESF for LabVIEW 2014</Property>
				<Property Name="PKG_version" Type="Str">1.2.2</Property>
			</Item>
		</Item>
	</Item>
</Project>
